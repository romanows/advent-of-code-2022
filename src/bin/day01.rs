//! Solution to [Advent of Code 2021 Day 1](https://adventofcode.com/2022/day/1).

use std::cmp;
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("day01.txt")[..] }; }

/// Return the largest sum of all blank-line separated chunks of integers, or `None`
/// if there are no integers in `calories`.
fn get_max_sum(calories: &[Option<u32>]) -> Option<u32> {
    // Iterate through the calories
    let (max, _) = calories.iter().fold((None, 0), |(max, acc), c| if let Some(x) = c { (Some(cmp::max(max.unwrap_or(0), acc + x)), acc + x) } else { (Some(cmp::max(max.unwrap_or(0), acc)), 0) });
    max
}

/// Return the sums for all blank-line separated chunks of integers. Returns an
/// empty `Vec` if there are no chunks of integers.
fn get_totals(calories: &[Option<u32>]) -> Vec<u32> {
    let mut totals = Vec::new();
    let mut acc = None;
    for c in calories {
        acc = match c {
            Some(x) => Some(acc.unwrap_or(0) + x),
            _ => { if let Some(v) = acc { totals.push(v) }; None }
        }
    }
    if let Some(v) = acc { totals.push(v) }
    totals
}

/// Return the sum of the three largest sums of all blank-line separated chunks of
/// integers. Returns `None` if there are not at least three elements in the given
/// `totals`.
fn sum_top3_totals(totals: &[u32]) -> Option<u32> {
    let mut totals: Vec<u32> = totals.to_vec();
    if totals.len() < 3 {
        None
    } else {
        totals.sort_unstable();
        totals.reverse();
        Some(totals.iter().take(3).sum())
    }
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(u32, u32)> {
    let calories: Vec<Option<u32>> = f.lines().map(|x| x.unwrap().parse::<u32>().ok()).collect();
    let ans_part_1 = get_max_sum(&calories).unwrap();
    let ans_part_2 = sum_top3_totals(&get_totals(&calories)).unwrap();
    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: Total calories carried by the elf with the most calories: {}", ans_part_1);
    println!("Part 2: Total calories carried by the top three elves with the most calories: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_given_part1() {
        assert_eq!(get_max_sum(&vec![Some(1000), Some(2000), Some(3000), None, Some(4000), None, Some(5000), Some(6000), None, Some(7000), Some(8000), Some(9000), None, Some(10000)]), Some(24000));
    }

    #[test]
    fn test_extra_part1() {
        assert_eq!(get_max_sum(&vec![]), None);
        assert_eq!(get_max_sum(&vec![None]), Some(0));
        assert_eq!(get_max_sum(&vec![None, None]), Some(0));
        assert_eq!(get_max_sum(&vec![Some(1)]), Some(1));
        assert_eq!(get_max_sum(&vec![Some(1), Some(1)]), Some(2));
        assert_eq!(get_max_sum(&vec![None, Some(3), None]), Some(3));
    }

    #[test]
    fn test_get_totals() {
        assert_eq!(get_totals(&vec![]), vec![]);
        assert_eq!(get_totals(&vec![None]), vec![]);
        assert_eq!(get_totals(&vec![None, None]), vec![]);

        assert_eq!(get_totals(&vec![Some(1)]), vec![1]);
        assert_eq!(get_totals(&vec![None, Some(1), None]), vec![1]);
        assert_eq!(get_totals(&vec![Some(1), None, Some(1), Some(1)]), vec![1, 2]);
    }

    #[test]
    fn test_given_part2() {
        assert_eq!(sum_top3_totals(&get_totals(&vec![Some(1000), Some(2000), Some(3000), None, Some(4000), None, Some(5000), Some(6000), None, Some(7000), Some(8000), Some(9000), None, Some(10000)])), Some(45000));
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        assert_eq!(main_quiet(f).unwrap(), (71502, 208191));
    }
}
