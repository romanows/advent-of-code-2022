# Overview
Using the [Advent of Code 2022](https://adventofcode.com/2022) as an excuse to
remember the little Rust I've forgotten during [2021](https://gitlab.com/romanows/advent-of-code-2021)
and [2019](https://gitlab.com/romanows/advent-of-code-2019).


# Run
After installing rust and cloning this repo, change directory into the project
root and run:
```sh
cargo run --bin day01 --release
```

...to see the answers for my data files printed to stdout.

There is some [generated documentation](https://romanows.gitlab.io/advent-of-code-2022).
